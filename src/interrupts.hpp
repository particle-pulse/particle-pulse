#pragma once

#include <stdint.h>
#include "defs.h"

EXTERN_C uint32_t handle_interrupt(uint8_t interruptNumber, uint32_t esp);

EXTERN_C void ignore_interrupt_request();
EXTERN_C void handle_interrupt_request0x00();
EXTERN_C void handle_interrupt_request0x01();

struct GateDescriptor {
	uint16_t handlerAddressLow;
	uint16_t gdtCodeSegmentSelector;
	uint8_t reserved;
	uint8_t access;
	uint16_t handlerAddressHigh;
} PACKED;

#define GATE_COUNT 256

struct GateDescriptor interruptDescriptorTable[GATE_COUNT];

void idt_set_entry(uint8_t interruptNumber, uint16_t codeSegmentSelectorOffset, void (*handler)(), uint8_t descriptorPrivilegeLevel, uint8_t descriptorType);

void idt_init();
