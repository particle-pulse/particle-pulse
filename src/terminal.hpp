#pragma once

#include <stdint.h>
#include <stddef.h>

enum VGAColor {
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	LightBrown = 14,
	White = 15,
};

static inline uint8_t vga_entry_color(VGAColor fg, VGAColor bg) {
	return static_cast<uint8_t>(fg) | static_cast<uint8_t>(bg) << 4;
}

static inline uint16_t vga_entry(unsigned char character, uint8_t color) {
	return static_cast<uint16_t>(character) | static_cast<uint16_t>(color) << 8;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

void terminal_initialize();

void terminal_clear();

void terminal_newline();

void terminal_setcolor(uint8_t color);

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y);

void terminal_putchar(char c);

void terminal_write(const char* data, size_t size);

void terminal_writestring(const char* data);


void cursor_enable(uint8_t cursor_start, uint8_t cursor_end);

void cursor_disable();

void cursor_update(size_t x, size_t y);
