/* Target Checking */
#include "check_target.hpp"
#include "defs.h"

#include "gdt.hpp"
#include "terminal.hpp"

EXTERN_C void kernel_main(void) {
	gdt_init();

	terminal_initialize();
	terminal_writestring("Hello, kernel World!\n");
}
