#ifndef EXTERN_H
#define EXTERN_H

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

#define PACKED __attribute__((packed))

#endif