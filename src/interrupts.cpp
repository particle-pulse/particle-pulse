#include "interrupts.hpp"
#include "kstdio.hpp"

uint32_t handle_interrupt(uint8_t interruptNumber, uint32_t esp) {
	kprint("INTERRUPT");
	return esp;
}

#define IDT_DESC_PRESENT 0x80

void idt_set_entry(uint8_t interruptNumber, uint16_t codeSegmentSelectorOffset, void (*handler)(), uint8_t descriptorPrivilegeLevel, uint8_t descriptorType) {
	interruptDescriptorTable[interruptNumber].handlerAddressLow = ((uint32_t) handler) & 0xFFFF;
	interruptDescriptorTable[interruptNumber].handlerAddressHigh = (((uint32_t) handler) >> 16) & 0xFFFF;
	interruptDescriptorTable[interruptNumber].gdtCodeSegmentSelector = codeSegmentSelectorOffset;
	interruptDescriptorTable[interruptNumber].access = IDT_DESC_PRESENT | descriptorType | ((descriptorPrivilegeLevel & 0x3) << 5);
	interruptDescriptorTable[interruptNumber].reserved = 0;
}

void idt_init() {
	//uint32_t codeSegment = 
}