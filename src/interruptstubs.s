.set IRQ_BASE, 0x20

.section .text

.extern handle_interrupt

.macro HandleException num
.global handle_exception\num
	movb $\num, (interruptnumber)
	jmp int_bottom
.endm

.macro HandleInterruptRequext num
.global handle_interrupt_request\num
	movb $\num + IRQ_BASE, (interruptnumber)
	jmp int_bottom
.endm

HandleInterruptRequext 0x00
HandleInterruptRequext 0x01

int_bottom:
	pusha
	pushl %ds
	pushl %es
	pushl %fs
	pushl %gs

	pushl %esp
	push (interruptnumber)
	call handle_interrupt
	movl %eax, %esp

	popl %gs
	popl %fs
	popl %es
	popl %ds
	popa

	iret

.data
	interruptnumber: .byte 0
